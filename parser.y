%{
	#include "global.h"
	extern symtable_struct* symtable;
	int yylex();
	void yyerror(const char*);

	//some prototypes
	int handleOp(int,int,int);
	int handleuOp(int,int);
	int handleRelOp(int, int, int);
	int handleSubprogramCall(int, int, bool);
	int handleSubprogramParameter(int*, int, int);
	void handleIOparam(int,int);

%}

%error-verbose

%token PROGRAM
%token VAR
%token BEGIN_TOKEN
%token END_TOKEN
%token ID
%token INTEGER
%token REAL
%token ARRAY
%token OF
%token ASSIGNOP
%token EQUALS NOTEQUALS LT LE GT GE
%token OR
%token AND DIVOP MODOP
%token NOT
%token IF THEN ELSE
%token WHILE DO
%token PROCEDURE FUNCTION
%token WRITE READ
%token DONE

%%
start		
	:	program DONE	{ YYACCEPT; }
	|	error			{ yyerror("error\n"); }
	;
program 	
	:	program_header ';'
		declarations
		subprogram_declarations
		program_label
		compound_stmt
		'.'				{ gencode0v("exit"); }
	;
program_header
	:	PROGRAM ID '(' identifier_list ')'	{ 
												int progLabel; 
												setProgramLabel($2);
												progLabel=newLabel();
												gencode1v("jump.i",progLabel,address);
												$$=progLabel; 
											}
	;
program_label
	:	/*empty*/	{ printLabel($-3); }
	;
identifier_list	
	:	ID
	|	identifier_list ',' ID
	;

declarations	
	:	declarations VAR ID array_start_marker type_width_marker identifier_list_with_type	{ symtable_addtype($3,$6,$5); symtable_setarray($3,$5>1,$4,$5); }
	|	/* empty */
	;
identifier_list_with_type	
	:	',' ID array_start_marker type_width_marker identifier_list_with_type	{ symtable_addtype($2,$5,$4); symtable_setarray($2,$4>1,$2,$4); $0=$4; $$=$5; }
	|	':'	type ';'	{ $$ = $2; }
	;
type
	:	simple_type		{ $-1=1; $-2 = 0; $$ = $1; }
	|	ARRAY '[' INTEGER ']' OF simple_type					{
																	$-1=symtable_entry($3)->value.ival;
																	$-2 = 0;
																	if($-1 <= 0) {
																		yyerror("Pojemnosc tablicy musi byc wieksza od 0.");
																	}
																	$$=$6; 
																}
	|	ARRAY '[' INTEGER '.' '.' INTEGER ']' OF simple_type	{
																	int startIndex = symtable_entry($3)->value.ival;
																	int endIndex = symtable_entry($6)->value.ival;
																	if(endIndex <= startIndex) {
																		yyerror("Poczatkowy indeks nie moze byc wiekszy badz rowny koncowemu.");
																	}
																	$-1 = endIndex - startIndex + 1; // width = end - start + 1 
																	$-2 = startIndex; // array_start = start
																	$$=$9;
																}
	;
simple_type 		
	:	INTEGER
	|	REAL
	;
type_width_marker
	: /* empty */
	;
array_start_marker
	: /* empty */
	;
subprogram_declarations
	:	subprogram_declarations subprogram_declaration ';'
	|	/* empty */
	;
subprogram_declaration
	:	subprogram_head declarations compound_stmt	{
														char* buffered;
														ob_end();
														buffered = ob_get();
														gencode1v("enter.i",getSubprogramLocalsOffset(),iliteral);
														gendirect(buffered);
														gencode0v("leave");
														gencode0v("return");
														free(buffered);
														symtable = symtable->parent;
													}
	;
subprogram_head
	:	FUNCTION ID function_symtab_mark argument_list ':' simple_type ';'		{ 
																					entry* parent_e = &symtable->parent->data[$2];
																					int retaddr = symtable_insert_id(parent_e->value.sval,ID);
																					parent_e->type = $6;
																					symtable->first_free_addr = 4;
																					symtable_addtype(retaddr,typePointer($6),-1);	// -1 cuz its local but param so in addtype fun it turn out --1 so +1
																					symtable->first_free_addr = 0;
																					ob_start();
																				}
	|	PROCEDURE ID procedure_symtab_mark argument_list ';'					{ symtable->first_free_addr = 0; ob_start();}
	;
function_symtab_mark
	:	/* empty */	{
						entry* f = symtable_entry($0);
						symtable_struct* st = newSymtable(symtable);
						printLabel($0);
						f->addr = (int) st;
						f->etype = function;
						st->first_free_addr = 8;	// because BP+0 is old BP, BP+4 is return (in this point of unknown type, but its pointer)
						symtable = st;
					}
	;
procedure_symtab_mark
	:	/* empty */	{
						entry* p = symtable_entry($0);
						symtable_struct* st = newSymtable(symtable);
						printLabel($0);
						p->addr = (int) st;
						p->etype = procedure;
						st->first_free_addr = 4; // because of BP on 0 offset with 4 bythes width
						symtable = st;
					}
	;
argument_list
	:	'(' parameter_list ')'
	|	/* empty */
	;
parameter_list
	:	ID array_start_marker type_width_marker param_id_list_with_type_prim { symtable_addtype($1,$4,-1); symtable_setarray($1,$3>1,$2,$3); }
	;
parameter_list_prim
	:	';' ID array_start_marker type_width_marker param_id_list_with_type_prim { symtable_addtype($2,$5,-1); symtable_setarray($2,$4>1,$3,$4); }
	|	/* empty */
	;
param_id_list_with_type_prim
	:	',' ID array_start_marker type_width_marker param_id_list_with_type_prim { symtable_addtype($2,$5,-1); symtable_setarray($2,$4>1,$3,$4); $-1 = $3; $0=$4; $$=$5; }
	|	':' type parameter_list_prim	{ $$ = typePointer($2); }
	;
compound_stmt	
	:	BEGIN_TOKEN
		optional_stmts
		END_TOKEN
	;
optional_stmts	
	:	stmt_list
	|	/* empty */
	;
stmt_list	
	:	stmt
	|	stmt_list ';' stmt
	;
stmt
	:	matched_stmt
	|	unmatched_stmt
	;
matched_stmt
	:	variable ASSIGNOP expression 	{
											char buf[10];
											int v_i = $1, e_i = $3;
											entry* v = symtable_entry(v_i);
											entry* e = symtable_entry(e_i);
											if(v->type != e->type && typePointer(v->type) != typePointer(e->type)) {
												switch(v->type) {
													case intptr:
													case integer:
														e_i = handleRealToInt(e_i);
														break;
													case realptr:
													case real:
														e_i = handleIntToReal(e_i);
														break;
												}
												e = symtable_entry(e_i);
											}
											sprintf(buf,"mov.%c",simpleType(v->type)==real?'r':'i');
											gencode2v(buf,e_i,value,v_i,value);
										}
	|	compound_stmt
	|	procedure_stmt
	|	WHILE while_true_label expression while_false_label DO matched_stmt		{
																					gencode1v("jump.i",$2,address);
																					printLabel($4);
																				}
	|	IF expression false_label THEN matched_stmt true_label ELSE matched_stmt {
																						printLabel($6);
																				}
	;
unmatched_stmt
	:	IF expression false_label THEN stmt {
												printLabel($3);
												//printf("%s:\n",symtable[$3].value.sval);
											}
	|	IF expression false_label THEN matched_stmt true_label ELSE unmatched_stmt {
																						printLabel($6);
																					}
	;
true_label
	:	/*empty*/{  
					int skip_label = newLabel();
					int false_label = $-2;
					gencode1v("jump.i",skip_label,address);
					printLabel(false_label);
					$$=skip_label;
				}
	;
false_label
	:	/*empty*/{ 
					int false_label = newLabel();
					int e_i = $0;
					entry* e = symtable_entry(e_i);
					if(simpleType(e->type) == real) {
						e_i = handleRealToInt(e_i);
						e = symtable_entry(e_i);
					}
					gencode("je.i",e_i,value,0,iliteral,false_label,address);
					$$=false_label;
				}
	;
while_true_label
	:	/*empty*/	{
						int true_label = newLabel();
						printLabel(true_label);
						$$ = true_label;
					}
	;
while_false_label
	:	/*empty*/	{
						int false_label = newLabel();
						int e_i = $0;
						entry* e = symtable_entry(e_i);
						if(simpleType(e->type) == real) {
							e_i = handleRealToInt(e_i);
							e = symtable_entry(e_i);
						}
						gencode("je.i",e_i,value,0,iliteral,false_label,address);
						$$ = false_label;
					}
	;
procedure_stmt
	:	ID 								{
											handleSubprogramCall($1,0,false);
										}
	|	ID '(' expression_list ')'		{
											handleSubprogramCall($1,$3,false);											
										}
	|	WRITE '(' expression_list ')'
	|	READ '(' expression_list ')'
	;
expression_list
	:	expression 						{
											if($-1 < 0)
												handleIOparam(-$-1,$1);
											else
												$$ = handleSubprogramParameter(&$-1,$1,0);
										}
	|	expression_list ',' expression  {
											if($-1 < 0)
												handleIOparam(-$-1,$3);
											else
												$$ = handleSubprogramParameter(&$-1,$3,$1);
										}
	;
variable
	:	ID 								{
											entry* i = symtable_entry($1);
											if(i->etype == procedure || i->etype == function) {
												int over = checkOvershadowing(i->value.sval,variable);
												if(over == NONE_FOUND)
													handleSubprogramCall($1,0,true);
												else
													$$ = over;	
											}
										}
	|	ID '[' expression ']'			{
											int a_i = $1, e_i = $3;
											entry* array = symtable_entry($1);
											entry* expr = symtable_entry($3);
											if(!array->array) {
												yyerror("Proba wyluskania zmiennej, a nie tablicy.");
											}
											int index_i = newTemp();
											int dst_i = newTemp();
											if(expr->type != integer) {
												e_i = handleRealToInt($3);
												expr = symtable_entry(e_i);
											}
											if(expr->etype == numeric) {
												if(expr->value.ival < array->array_start) {
													yyerror("Indeks tablicy musi byc wiekszy badz rowny zadeklarowanemu indeksowi jej poczatku.");
												}
											}
											symtable_addtype(index_i, integer, 1);
											symtable_addtype(dst_i, typePointer(array->type), 1);
											gencode("sub.i",e_i,value,array->array_start,iliteral,index_i,value);
											gencode("mul.i",index_i,value,symtable_insert_int(typeWide(array->type)),value,index_i,value);
											gencode("add.i",a_i,address,index_i,value,dst_i,address);
											$$ = dst_i;
										}
	;
expression
	:	simple_expression
	|	simple_expression relop	simple_expression 	{
														$$=handleRelOp($2,$1,$3);
													}
	;
relop
	:	EQUALS
	|	NOTEQUALS
	|	GT
	|	LT 
	|	GE
	|	LE
	;
simple_expression
	:	term
	|	sign term						{
											$$=handleuOp($1,$2);
										}
	|	simple_expression sign term		{
											$$=handleOp($2,$1,$3);
										}
	|	simple_expression OR term		{
											$$=handleOp($2,$1,$3);
										}
	;
sign
	:	'+'
	|	'-'
	;
term
	:	factor
	|	term mulop factor				{
											$$=handleOp($2,$1,$3);
										}
	;
factor
	:	variable
	|	ID '(' expression_list ')'		{
											$$ = handleSubprogramCall($1,$3,true);
										}
	|	INTEGER
	|	REAL
	|	'(' expression ')'	{ $$ = $2; }
	|	NOT factor						{
											int f_i = $2;
											int dst_i = newTemp();
											int skip_label = newLabel(), false_label = newLabel();
											entry* f = symtable_entry(f_i);
											symtable_addtype(dst_i,integer,1);
											if(simpleType(f->type) != integer) {
												f_i = handleRealToInt(f_i);
												f = symtable_entry(f_i);
											}
											gencode("je.i",f_i,value,0,iliteral,false_label,address);
											gencode2v("mov.i",1,iliteral,dst_i,value);
											gencode1v("jump.i",skip_label,address);
											printLabel(false_label);
											gencode2v("mov.i",0,iliteral,dst_i,value);
											printLabel(skip_label);
											$$ = dst_i;
										}
	;
mulop
	:	'*'
	|	'\\'
	|	DIVOP
	|	MODOP
	|	AND
	;
%%
void parse(){
	yyparse();
}

void yyerror(const char* s) {
	fprintf(stderr,"lookup: %d\n",yychar);
	error(s);
}

int handleOp(int op, int o1_i, int o2_i) {
	char buf[10];
	char *inst;
	entry* o1 = symtable_entry(o1_i);
	entry* o2 = symtable_entry(o2_i);
	int dst_i = newTemp();
	entry* dst = symtable_entry(dst_i);
	if(o1->type == o2->type || typePointer(o1->type) == typePointer(o2->type)) {
		symtable_addtype(dst_i, simpleType(o1->type), 1);
	}
	else {
		symtable_addtype(dst_i, real, 1);
		if(simpleType(o1->type) == integer) {
			o1_i = handleIntToReal(o1_i);
			o1 = symtable_entry(o1_i);
		}
		else{
			o2_i = handleIntToReal(o2_i);
			o2 = symtable_entry(o2_i);
		}
	}
	switch(op) {
		case '+':
			inst = "add";
			break;
		case '-':
			inst = "sub";
			break;
		case OR:
			inst = "or";
			break;
		case '*':
			inst = "mul";
			break;
		case '\\':
			inst = "div";
			break;
		case MODOP:
			inst = "mod";
			break;
		case DIVOP:
			inst = "div";
			break;
		case AND:
			inst = "and";
			break;
	}
	sprintf(buf,"%s.%c",inst,dst->type==real?'r':'i');
	gencode(buf,o1_i,value,o2_i,value,dst_i,value);
	return dst_i;
}

int handleuOp(int op, int o_i) {
	char buf[10];
	char *inst;
	entry* o = symtable_entry(o_i);
	int dst_i = newTemp();
	entry* dst = symtable_entry(dst_i);
	symtable_addtype(dst_i,o->type,1);
	switch(op) {
		case '-':
			inst = "sub";
			break;
		case '+':
			inst = "add";
			break;
	}
	sprintf(buf,"%s.%c",inst,dst->type==real?'r':'i');
	gencode(buf,
		dst->type == real ? (int) 0.f : 0, dst->type == real ? rliteral : iliteral,
		o_i, value,
		dst_i, value
	);
	return dst_i;
}

int handleIntToReal(int o_i) {
	int dst_tmp_i = newTemp();
	symtable_addtype(dst_tmp_i, real, 1);
	gencode2v("inttoreal.i",o_i,value,dst_tmp_i,value);
	return dst_tmp_i;

}
int handleRealToInt(int e_i) {
	int t_i = newTemp();
	symtable_addtype(t_i, integer, 1);
	gencode2v("realtoint.r",e_i,value,t_i,value);
	return t_i;
}

int handleRelOp(int op, int o1_i, int o2_i){
	char buf[15];
	char *inst;
	entry* o1 = symtable_entry(o1_i);
	entry* o2 = symtable_entry(o2_i);
	int dst_i = newTemp();
	entry* dst = symtable_entry(dst_i);
	int skip_label = newLabel(), true_label = newLabel();
	symtable_addtype(dst_i, integer, 1);
	if(o1->type != o2->type && typePointer(o1->type) != typePointer(o2->type)) {
		if(simpleType(o1->type) == real) {
			o1_i = handleRealToInt(o1_i);
			o1 = symtable_entry(o1_i);
		}
		else{
			o2_i = handleRealToInt(o2_i);
			o2 = symtable_entry(o2_i);
		}
	}
	switch(op) {
		case LT:
			inst = "jl";
			break;
		case GT:
			inst = "jg";
			break;
		case LE:
			inst = "jle";
			break;
		case GE:
			inst = "jge";
			break;
		case EQUALS:
			inst = "je";
			break;
		case NOTEQUALS:
			inst = "jne";
			break;
	}
	sprintf(buf,"%s.%c",inst,dst->type==real?'r':'i');
	gencode(buf,o1_i,value,o2_i,value,true_label,address);
	gencode2v("mov.i",0,iliteral,dst_i,value);
	gencode1v("jump.i",skip_label,address);
	printLabel(true_label);
	gencode2v("mov.i",1,iliteral,dst_i,value);
	printLabel(skip_label);
	return dst_i;
}

int handleSubprogramCall(int s_i, int param_count, bool must_return) {
	entry* s = symtable_entry(s_i);
	int r_i;
	char buf[15];
	int reported_param_count;
	if(s->etype != function && s->etype != procedure) {
		yyerror("Proba uzycia zminnej w miejsce funkcji/procedury.");
	}
	reported_param_count = getSubProgramParametersCount((symtable_struct*)symtable_entry(s_i)->addr, s->etype == function);
	if(reported_param_count != param_count) {
		printf("Podano %d parametrow, potrzeba %d.\n",param_count,reported_param_count);
		yyerror("Za mala ilosc parametrow");
	}
	if(s->etype == function) {
		r_i = newTemp();
		param_count = reported_param_count+1;
		symtable_addtype(r_i,s->type,1);
		sprintf(buf,"%s.%c","push",s->type==real?'r':'i');
		gencode1v(buf,r_i,address);
	}
	else {
		if(must_return) {
			yyerror("Proba uzycia procedury w miejscu funkcji.");
		}
	}
	gencode1v("call.i",s_i,address);
	gencode1v("incrsp.i",param_count*4,iliteral);
	return r_i;
}

int handleSubprogramParameter(int* sub_ip, int e_i, int param_i) {
	int sub_i = *sub_ip;
	entry* sub_parent = symtable_entry(sub_i);
	symtable_struct* sub;
	int sub_param_i;
	entry* param;
	entry* e = symtable_entry(e_i);
	char buf[15];
	if(sub_parent->etype == numeric) {
		yyerror("Proba uzycia stalej w miejscu funkcji")	;
	}
	if(sub_parent->etype != function && sub_parent->etype != procedure) {
		sub_i = checkOvershadowing(sub_parent->value.sval, function);
		if(sub_i == NONE_FOUND)
			sub_i = checkOvershadowing(sub_parent->value.sval, procedure);
		if(sub_i == NONE_FOUND)
			yyerror("Proba uzycia zminnej w miejsce funkcji/procedury.");
		sub_parent = symtable_entry(sub_i);
		*sub_ip = sub_i;
	}
	sub = (symtable_struct*) sub_parent->addr;
	sub_param_i = getSubProgramParameter(sub,param_i,sub_parent->etype==function);
	if(sub_param_i != NONE_FOUND) {
		param = &sub->data[sub_param_i];
		if(param->array) {
			if(e->array) {
				if(typePointer(param->type) != typePointer(e->type)) {
					yyerror("Niezgodnosc typow tablic");
				}
				//check range
				if(param->array_cap != e->array_cap) {
					yyerror("Tablice maja rozne pojemnosci");
				}
			}
			else
				yyerror("Parametr musi byc tablica.");
		}
		if(e->etype == numeric) {	//convert numeric to variable, cuz function needs pointers
			int t_i = e_i;
			e_i = newTemp();
			symtable_addtype(e_i,e->type,1);
			sprintf(buf,"%s.%c","mov",e->type==real?'r':'i');
			gencode2v(buf,t_i,value,e_i,value);
			e = symtable_entry(e_i);

		}
		if(param->type != e->type && typePointer(param->type) != typePointer(e->type)) {
			switch(e->type) {
				case intptr:
				case integer:
					e_i = handleRealToInt(e_i);
					break;
				case realptr:
				case real:
					e_i = handleIntToReal(e_i);
					break;
			}
			e = symtable_entry(e_i);
		}
		sprintf(buf,"%s.%c","push",e->type==real?'r':'i');
		gencode1v(buf,e_i,address);
	}
	else {
		yyerror("Wywolano podprogram z za duza iloscia parametrow.");
	}
	return param_i+1;
}

void handleIOparam(int io, int param_i) {
	char buf[15];
	entry* e = symtable_entry(param_i);
	char* inst = io == WRITE ? "write" : "read";
	sprintf(buf,"%s.%c",inst,simpleType(e->type)==real?'r':'i');
	gencode1v(buf,param_i,value);
}