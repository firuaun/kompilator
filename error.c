#include "global.h"

void error(const char* msg) {
	fprintf(stderr,"parser error: on line %d column %d; %s\n",lineno, colno, msg);
	freeAllocedSymtables();
	exit(1);
}