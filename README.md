## kompilator

hope it works


### Lista rzeczy problematycznych:
* brak notacji wykładniczej w lekserze;
* zmienona definicja tablicy: zamiast `array[num1..num2] of typ` jest `array[num] of typ` implikujac zapis `array[0..num2] of typ`,
* zmienione w kodzie wynikowym wyłuskiwanie na [address] zamiast *address;
* odwrócone offsety adresów zmiennych przy deklaracji, tj. `var x,y:integer;` produkuje *x* z offsetem **4**, a *y* z **0**;
* lepiej działające ify (chyba xD), tzn. nie ma problemu z zagnieżdżonym if elsem, ale jest pełna ewaluacja;
* coś napewno nie jest jeszcze ok.

### Do zrobienia (oprócz oczywistych spraw):
* ~~wrzucić po jednym zerze i jedynce do tablicy symboli~~ (lepiej zrobić literały xD),
* ~~dodatkowe info przy wyświetlaniu tablicy symboli~~,
* ~~tablica symboli powinna wyświetlać nazwy typów zmiennych, a nie numery enumów~~,
* ~~zapisywanie do pliku, a w razie braku podanej nazwy jako argument, do stdout~~,
* ~~dodać dodatkową zmienną do entry, by sprawdzać czy wpis jest tablicą~~,
* ~~sprawdzanie wyłuskania tablic i zgodności parametrów wywołania~~,
* ~~wprowadzić słowa kluczowe write i read~~,
* ~~rekurencja (używanie funkcji o tej samej nazwie - problem ze zmienna zwracajającą)~~,
* sprawdzenie i wychwytywanie błędów (tj. używanie niezadeklarowanych zmiennych, etc.),
* ~~pascalowy styl arrayów (może w innym branchu)~~,
* szukanie problemów w samym kompilatorze.

### Ciekawostki:
* Dostarczony z zajęć kompilator "referencyjny" generuje instrukcje porównujący zawsze inty.
* Ten sam kompilator nie rozróżnia znaku zmiennej/literału, to znaczy: `a := +1` jest tym samym co `a := -1`.