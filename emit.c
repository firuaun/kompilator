#include "global.h"

extern symtable_struct* symtable;

const char* varTypeName(vartype);

void gencode(char* instr, int v1, varmode v1m, int v2, varmode v2m, int v3, varmode v3m) {
	int i;
	entry* e[3];
	varmode v[3];
	int c[3];
	char buf[3][20];
	c[0] = v1;
	e[0] = v1 != NONE ? symtable_entry(v1) : NULL;
	v[0] = v1m;
	c[1] = v2;
	e[1] = v2 != NONE ? symtable_entry(v2) : NULL;
	v[1] = v2m;
	c[2] = v3;
	e[2] = v3 != NONE ? symtable_entry(v3) : NULL;
	v[2] = v3m;
	for(i=0; i<3 && e[i] != NULL; i++) {
		switch(v[i]) {
			case address:
				switch(e[i]->etype) {
					case variable:
						switch(e[i]->type) {
							case real:
							case integer:
								sprintf(buf[i],"#%d",e[i]->addr);
								break;
							case intptr:
							case realptr:
								sprintf(buf[i],"%d",e[i]->addr);
						}
						break;
					case function:
					case procedure:
					case label:
						sprintf(buf[i],"#%s",e[i]->value.sval);
						break;
					case local:
						if(e[i]->addr > 0) {
							sprintf(buf[i],"BP+%d",e[i]->addr);	// always a pointer, cuz its parameter
						}
						else {
							switch(e[i]->type) {
								case real:
								case integer:
									sprintf(buf[i],"#BP-%d",-e[i]->addr);
									break;
								case intptr:
								case realptr:
									sprintf(buf[i],"BP-%d",-e[i]->addr);
							}
						}
						break;
					case numeric:
					case none:
					default:
						error("Proba pobrania adresu stalej lub niezaincjalizowanej zmiennej.");
						break;
				}				
				break;
			case value:
				switch(e[i]->etype) {
					case variable:
						switch(e[i]->type) {
							case real:
							case integer:
								sprintf(buf[i],"%d",e[i]->addr);
								break;
							case intptr:
							case realptr:
								sprintf(buf[i],"[%d]",e[i]->addr);
						}
						break;
					case function:
					case procedure:
					case label:
						sprintf(buf[i],"%s",e[i]->value.sval);
						break;
					case local:
						if(e[i]->addr > 0) {
							sprintf(buf[i],"[BP+%d]",e[i]->addr);	// always a pointer, cuz its parameter
						}
						else {
							switch(e[i]->type) {
								case real:
								case integer:
									sprintf(buf[i],"BP-%d",-e[i]->addr);
									break;
								case intptr:
								case realptr:
									sprintf(buf[i],"[BP-%d]",-e[i]->addr);
							}
						}
						break;
					case numeric:
						switch(e[i]->type) {
							case real:
								sprintf(buf[i],"#%.2f",e[i]->value.rval);
								break;
							case integer:
								sprintf(buf[i],"#%d",e[i]->value.ival);
								break;
							case intptr:
							case realptr:
								sprintf(buf[i],"[%d]",e[i]->addr);
						}
						break;
					case none:
					default:
						error("Proba uzycia niezaincjalizowanej zmiennej/stalej.");
				}
				break;
			case iliteral:
				sprintf(buf[i],"#%d",c[i]);
				break;
			case rliteral:
				sprintf(buf[i],"#%.2f",(float)c[i]);
				break;
		}
	}
	if(e[1] == NULL) {
		fprintf(out,"\t%s %s\n",instr,buf[0]);
	}else if(e[2]==NULL) {
		fprintf(out,"\t%s %s, %s\n",instr,buf[0],buf[1]);
	}
	else {
		fprintf(out,"\t%s %s, %s, %s\n",instr,buf[0],buf[1],buf[2]);
	}


}
void gencode0v(char* instr) {
	fprintf(out,"\t%s\n",instr);
}
void gencode1v(char* instr, int v1, varmode v1m) {
	gencode(instr,v1,v1m,NONE,NONE,NONE,NONE);
}
void gencode2v(char* instr, int v1, varmode v1m, int v2, varmode v2m) {
	gencode(instr,v1,v1m,v2,v2m,NONE,NONE);
}

void dumpGlobalSymbolTable() {
	int i,l=getSymbolTableSize();
	printf("Zrzut globalnej tablicy symboli:\n");
	for(i=0;i<=l;i++){
		entry* e = symtable_entry(i);
		switch(e->etype) {
			case numeric:
				if(e->type == integer) {
					printf("[%d] wartosc numeryczna %d typu integer.\n",i,e->value.ival);
				}
				else {
					printf("[%d] wartosc numeryczna %g typu real.\n",i,e->value.rval);
				}
				break;
			case variable:
				printf("[%d] globalna zmienna \"%s\" typu%s%s, address %d.\n",i,e->value.sval,e->array ? " tablica ":" ",varTypeName(e->type),e->addr);
				break;
			case label:
				printf("[%d] globalna etykieta \"%s\".\n",i,e->value.sval);
				break;
			case procedure:
				printf("[%d] globalna procedura \"%s\".\n",i,e->value.sval);
				break;
			case function:
				printf("[%d] globalna funkcja \"%s\" zwracająca typ %s.\n",i,e->value.sval,varTypeName(e->type));
				break;
			case local:
			case none:
			default:
				break;
		}
	}
}

int printLabel(int i) {
	fprintf(out,"%s:\n",symtable_entry(i)->value.sval);
	return i;
}

const char* varTypeName(vartype type) {
	switch(type) {
		case real:
			return "real";
		case integer:
			return "integer";
			break;
		case intptr:
			return "wskaźnik na integer";
		case realptr:
			return "wskaźnik na real";
	}
	return "none";
}

void dumpAllLocalSymbolTables() {
	int i,l=getSymbolTableSize();
	for(i=0;i<=l;i++){
		entry* e = symtable_entry(i);
		if(e->etype == procedure || e->etype == function) {
			int j;
			symtable_struct* st = (symtable_struct*) e->addr;
			printf("Zrzut tablicy symboli dla %s \"%s\":\n",e->etype == procedure ? "procedury" : "funkcji",e->value.sval);
			for(j=0;j<=st->last_symtable_entry_pos;j++){ 
				entry* le = &st->data[j];
				switch(le->etype) {
					case local:
						if(le->addr > 0) {
							printf("[%d] lokalny parametr \"%s\" typu%s%s, address BP+%d.\n",j,le->value.sval,le->array ? " tablica ":" ",varTypeName(le->type),le->addr);
						}
						else {
							printf("[%d] lokalna zmienna \"%s\" typu%s%s, address BP%d.\n",j,le->value.sval,le->array ? " tablica ":" ",varTypeName(le->type),le->addr);
						}
						break;
					case numeric:
						if(le->type == integer) {
							printf("[%d] wartosc numeryczna %d typu integer.\n",j,le->value.ival);
						}
						else {
							printf("[%d] wartosc numeryczna %g typu real.\n",j,le->value.rval);
						}
						break;
					case label:
						printf("[%d] lokalna etykieta \"%s\".\n",j,le->value.sval);
						break;
					case variable:
					case procedure:
					case function:
					case none:
						break;
				}
			}
		}
	}
}

void gendirect(char* src) {
	fprintf(out,"%s",src);
}

char* tout_buffer;
size_t tout_size;
FILE* out_stack[5];
int out_top = -1;

void out_push(FILE* o) {
	out_stack[++out_top] = o;
}

FILE* out_pop() {
	return out_stack[out_top--];
}

void ob_start() {
	FILE* tout;
	tout = open_memstream(&tout_buffer, &tout_size);
	out_push(out);
	out = tout;
}

void ob_end() {
	FILE* prev_out = out_pop();
	fflush(out);
	fclose(out);
	out = prev_out;
}

char* ob_get() {
	return tout_buffer;
}