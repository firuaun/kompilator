%{
	#include "global.h"
%}

ws		[ \t]+
nl		[\n]
letter 	[A-Za-z]
digit 	[0-9]	
id 		{letter}({letter}|{digit})*
integer	{digit}+
real	{integer}\.{integer}

%%
{ws}		{colno+=yyleng;}
{nl}		{lineno++;colno=1;}
"program"	{colno+=yyleng;return(PROGRAM);}
"var"		{colno+=yyleng;return(VAR);}
"begin"		{colno+=yyleng;return(BEGIN_TOKEN);}
"end"		{colno+=yyleng;return(END_TOKEN);}
"integer"	{colno+=yyleng;yylval=integer;return(INTEGER);}
"real"		{colno+=yyleng;yylval=real;return(REAL);}
"array"		{colno+=yyleng;return(ARRAY);}
"of"		{colno+=yyleng;return(OF);}
"if"		{colno+=yyleng;return(IF);}
"then"		{colno+=yyleng;return(THEN);}
"else"		{colno+=yyleng;return(ELSE);}
"while"		{colno+=yyleng;return(WHILE);}
"do"		{colno+=yyleng;return(DO);}
":="		{colno+=yyleng;return(ASSIGNOP);}
"="			{colno+=yyleng;yylval=EQUALS;return(EQUALS);}
"<>"		{colno+=yyleng;yylval=NOTEQUALS;return(NOTEQUALS);}
"<"			{colno+=yyleng;yylval=LT;return(LT);}
">"			{colno+=yyleng;yylval=GT;return(GT);}
"<="		{colno+=yyleng;yylval=LE;return(LE);}
">="		{colno+=yyleng;yylval=GE;return(GE);}
"or"		{colno+=yyleng;yylval=OR;return(OR);}
"div"		{colno+=yyleng;yylval=DIVOP;return(DIVOP);}
"mod"		{colno+=yyleng;yylval=MODOP;return(MODOP);}
"and"		{colno+=yyleng;yylval=AND;return(AND);}
"not"		{colno+=yyleng;yylval=NOT;return(NOT);}
"procedure"	{colno+=yyleng;return(PROCEDURE);}
"function"	{colno+=yyleng;return(FUNCTION);}
"write"		{colno+=yyleng;yylval=-WRITE;return(WRITE);}
"read"		{colno+=yyleng;yylval=-READ;return(READ);}
{id}		{yylval=install_id();colno+=yyleng;return(ID);}
{integer}	{yylval=install_num(INTEGER); colno+=yyleng; return(INTEGER);}
{real}		{yylval=install_num(REAL); colno+=yyleng; return(REAL);}
<<EOF>>		{return(DONE);}
.			{colno++;yylval=(int)yytext[0];return yylval;}

%%

int install_id() {
	int p;
	if(yyleng >= STR_MAX_SIZE)
		error("lexan error: yyleng too long");
	p = symtable_lookup(yytext);
	if(p == NONE_FOUND) {
		p = symtable_insert_id(newString(yytext),ID);
	}
	return p;
}

int install_num(int token) {
	int p;
	int ival;
	double dval;
	switch(token) {
		case INTEGER:
			sscanf(yytext,"%d",&ival);
			p = symtable_insert_int(ival);
			break;
		case REAL:
			sscanf(yytext,"%lf",&dval);
			p = symtable_insert_real(dval);
			break;
		default:
			error("lexan error: unknown token during num installation");
	}
	return p;

}

int lexan(){
	return yylex();
}

void lchangein(FILE* f){
	yyin = f;
}

int yywrap() {
	
	return 1;
}