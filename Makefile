TARGET = comp
LIBS =
CC = gcc
CFLAGS = -g -Wall
FLEX_OBJECT = lexer.c
FLEX_TARGET = lexer.o
BISON_OBJECT = parser.c
BISON_TARGET = parser.o

.PHONY: default all clean

default: $(TARGET)
all: default

OBJECTS = $(filter-out  $(BISON_TARGET) $(FLEX_TARGET), $(patsubst %.c, %.o, $(wildcard *.c)))
HEADERS = $(wildcard *.h)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

$(FLEX_OBJECT): lexer.l
	flex -o $(FLEX_OBJECT) lexer.l

$(FLEX_TARGET): $(FLEX_OBJECT)
	gcc -c $(FLEX_OBJECT) -o $(FLEX_TARGET)

$(BISON_OBJECT): parser.y
	bison --defines=parser.tab.h -o $(BISON_OBJECT) parser.y

$(BISON_TARGET): $(BISON_OBJECT)
	gcc -c $(BISON_OBJECT) -o $(BISON_TARGET)

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(BISON_TARGET) $(BISON_OBJECT) $(OBJECTS)  $(FLEX_TARGET) $(FLEX_OBJECT)
	$(CC) $(BISON_TARGET) $(OBJECTS) $(FLEX_TARGET) -Wall $(LIBS) -o $@

clean:
	-rm -f *.o
	-rm -f $(TARGET)
	-rm -f *.a
	-rm -f *.so
	-rm -f $(FLEX_OBJECT) $(FLEX_TARGET)
	-rm -f $(BISON_TARGET) $(BISON_OBJECT) parser.tab.h
