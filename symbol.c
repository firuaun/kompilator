#include "global.h"

#define SYMMAX 100
#define STRMAX 999

char strdata[STRMAX];	// pool for all string data
char buffer[STR_MAX_SIZE];
int first_free_strdata_pos = 0;
entry symtable_data[SYMMAX];	// global symbol table data
symtable_struct* symtable = &(symtable_struct){NULL, -1, 0, symtable_data};		// symbol table
int tmp_first_free_num = 0;
char* label_prefix;
int label_first_free_num = 0;

/* prototypes */
int copy_to_current_symtable(symtable_struct*, int);
int specific_symtable_lookup(const char*, symtable_struct*);


entry* symtable_entry(int i) {
	return &(symtable->data[i]);
}

int specific_symtable_lookup(const char* name, symtable_struct* st) {
	int i;
	for(i=st->last_symtable_entry_pos; i >= 0; i--) {
		entry* l = &st->data[i];
		if(l->token == ID && strcmp(l->value.sval,name) == 0) {
			return i;
		}
	}
	return NONE_FOUND;
}
int copy_to_current_symtable(symtable_struct* st, int i) {
	if(symtable->last_symtable_entry_pos + 1 >= SYMMAX) {
		error("ERROR: space for symbols is exceeded\n");
	}
	symtable->last_symtable_entry_pos++;
	memcpy(&symtable->data[symtable->last_symtable_entry_pos],&st->data[i],sizeof(entry));
	return symtable->last_symtable_entry_pos;
}
int symtable_lookup(const char* name) {
	int i;
	symtable_struct* tmp_st = symtable;
	i = specific_symtable_lookup(name, symtable);
	if(i == NONE_FOUND) {
		while(tmp_st->parent != NULL) {
			tmp_st = tmp_st->parent;
			i = specific_symtable_lookup(name, tmp_st);
			if(i != NONE_FOUND) {
				return copy_to_current_symtable(tmp_st,i);
			}
		} 
	}
	else {
		return i;
	}
	return NONE_FOUND;
}

int symtable_insert_id(char* name, int token) {
	entry* e;
	if(symtable->last_symtable_entry_pos + 1 >= SYMMAX) {
		error("ERROR: space for symbols is exceeded\n");
	}
	symtable->last_symtable_entry_pos++;
	e = symtable_entry(symtable->last_symtable_entry_pos);
	e->token = token;
	e->etype = none;
	e->value.sval = name;
	return symtable->last_symtable_entry_pos;
}

int symtable_insert_int(int value) {
	entry* e;
	if(symtable->last_symtable_entry_pos + 1 >= SYMMAX) {
		error("ERROR: space for symbols is exceeded\n");
	}
	symtable->last_symtable_entry_pos++;
	e = symtable_entry(symtable->last_symtable_entry_pos);
	e->token = INTEGER;
	e->type = integer;
	e->etype = numeric;
	e->value.ival = value;
	return symtable->last_symtable_entry_pos;

}

int symtable_insert_real(double value){
	entry* e;
	if(symtable->last_symtable_entry_pos + 1 >= SYMMAX) {
		error("ERROR: space for symbols is exceeded\n");
	}
	symtable->last_symtable_entry_pos++;
	e = symtable_entry(symtable->last_symtable_entry_pos);
	e->token = REAL;
	e->type = real;
	e->etype = numeric;
	e->value.rval = value;
	return symtable->last_symtable_entry_pos;

}

int symtable_addtype(int symtab_index, vartype type, int width) {
	int multipier = 4;
	entry* e = symtable_entry(symtab_index);
	e->type = type;
	e->addr = symtable->first_free_addr;
	e->etype = symtable->parent == NULL ? variable : local;
	if(type == real)
		multipier = 8;
	if(e->etype == local) {
		width = -width;
		if(width < 0)
			e->addr += width*multipier;		// count offset for local variables, but not parameters
	}
	symtable->first_free_addr += width*multipier;
	return symtable->first_free_addr;
}
int symtable_setarray(int i, bool array, int start, int cap) {
	symtable->data[i].array = array;
	if(array) {
		symtable->data[i].array_start = start;
		symtable->data[i].array_cap = cap;
	}
	return i;
}

char* newString(char* src) {
	char* dst;
	int len = strlen(src);
	if(first_free_strdata_pos + len + 1 >= STRMAX) {
		error("ERROR: space for string symbols is exceeded\n");
	}
	dst = &strdata[first_free_strdata_pos];
	first_free_strdata_pos += len + 1;
	strcpy(dst, src);
	return dst;
}

int newTemp() {
	sprintf(buffer,"$t%d",tmp_first_free_num++);
	return symtable_insert_id(newString(buffer),ID);
}

int newLabel() {
	int index;
	sprintf(buffer,"%s%d",label_prefix,label_first_free_num++);
	index = symtable_insert_id(newString(buffer),ID);
	symtable->data[index].etype = label;
	return index;
}

void setProgramLabel(int i){
	label_prefix = symtable_entry(i)->value.sval;
}

vartype typePointer(vartype prim) {
	switch(prim) {
		case integer: return intptr;
		case real: return realptr;
		case intptr: return intptr;
		case realptr: return realptr;
	}
	return -1;
}

vartype simpleType(vartype prim) {
	switch(prim) {
		case intptr:
		case integer: return integer;
		case real:
		case realptr: return real;
	}
	return -1;
}

int typeWide(vartype prim) {
	switch(prim) {
		case intptr:
		case realptr: 
		case integer: return 4;
		case real: return 8;
	}
	return 4;
}

int getSymbolTableSize() {
	return symtable->last_symtable_entry_pos;
}

symtable_struct* newSymtable(symtable_struct* parent){
	symtable_struct* st = (symtable_struct*) malloc(sizeof(symtable_struct));
	st->parent = parent;
	st->first_free_addr = 0;
	st->last_symtable_entry_pos = -1;
	st->data = (entry*) malloc(sizeof(entry)*SYMMAX);
	return st;
}

void delSymtable(symtable_struct* st) {
	free(st->data);
	free(st);
}

void freeAllocedSymtables() {
	int i,l=getSymbolTableSize();
	for(i=0;i<=l;i++){
		entry* e = symtable_entry(i);
		if(e->etype == procedure || e->etype == function) {
			delSymtable((symtable_struct*)e->addr);
		}
	}
}

int getSubprogramLocalsOffset() {
	int i,l=getSymbolTableSize();
	int sum = 0;
	for(i=0;i<=l;i++){
		entry* e = symtable_entry(i);
		if(e->etype == local && e->addr < 0) {
			sum += typeWide(e->type);
		}
	}
	return sum;
}

int getSubProgramParameter(symtable_struct* st, int number, bool is_f) {
	int i,count=-1;
	int param_offset = is_f ? 4 : 0;
	for(i = 0; i < st->last_symtable_entry_pos; i++) {
		entry* e = &st->data[i];
		if(e->etype == local) {
			if(e->addr > param_offset) {
				if(number == ++count) {
					return i;
				}
			}
			else {
				return NONE_FOUND;
			}
		}
	}
	return NONE_FOUND;
}

int getSubProgramParametersCount(symtable_struct* st, bool is_f) {
	int i;
	int count=0;
	int param_cutoff = is_f ? 4 : 0;
	for(i = 0; i < st->last_symtable_entry_pos; i++) {
		entry* e = &st->data[i];
		if(e->etype == local) {
			if(e->addr > param_cutoff) {
				count++;
			}
			else {
				break;
			}
		}
	}
	return count;
}

int checkOvershadowing(const char* name, entry_type lookingfor) {
	int i;
	symtable_struct* tmp_st = symtable;
	for(i=tmp_st->last_symtable_entry_pos; i >= 0; i--) {
		entry* l = &tmp_st->data[i];
		if(l->token == ID && strcmp(l->value.sval,name) == 0 && l->etype == lookingfor) {
			return i;
		}
	}
	while(tmp_st->parent != NULL) {
		tmp_st = tmp_st->parent;
		for(i=tmp_st->last_symtable_entry_pos; i >= 0; i--) {
			entry* l = &tmp_st->data[i];
			if(l->token == ID && strcmp(l->value.sval,name) == 0 && l->etype == lookingfor) {
				return copy_to_current_symtable(tmp_st,i);
			}
		}
	}
	return NONE_FOUND;	
}