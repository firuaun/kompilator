#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <stdbool.h>
#include "parser.tab.h"

#define STR_MAX_SIZE 69
#define NONE_FOUND -1
#define NONE -1

FILE* out;

/* initialize variables essential for error stack trace */
int lineno;
int colno;

/* defined in init.c */
void init();

/* defined in lexer.l */
int lexan();
inline void lchangein(FILE*);

/* defined in parser.y */
void parse();

/* defined in error.c */
void error(const char*);

typedef enum {
	address, value, iliteral, rliteral
} varmode;
/*defined in emit.c */
void gencode(char*,int,varmode,int,varmode,int, varmode);
inline void gencode0v(char*);
inline void gencode1v(char*, int, varmode);
inline void gencode2v(char*, int, varmode, int, varmode);

inline void gendirect(char*);

inline int printLabel(int);
void dumpGlobalSymbolTable();
void dumpAllLocalSymbolTables();

/* This buffers output to string (helpful on function/procedure enter instruction) */
void ob_start();
void ob_end();
inline char* ob_get();

typedef enum {
	none, numeric, variable, label, procedure, function, local
} entry_type;
/* possible types for variables */
typedef enum {
	integer, real, intptr, realptr
} vartype;

/* symbol table entry structure */
typedef struct symtable_struct symtable_struct;

typedef union {
	int ival;
	double rval;
	char* sval;
} attrval;

typedef struct {
	int token;
	attrval value;
	vartype type;
	int addr;
	entry_type etype;
	bool array;
	int array_start;
	int array_cap;
} entry;

struct symtable_struct {
	symtable_struct* parent;
	int last_symtable_entry_pos;
	int first_free_addr;
	entry* data;
};

/* defined in symbol.c */
inline entry* symtable_entry(int);
int symtable_lookup(const char*);
int symtable_insert_id(char*, int);
int symtable_insert_int(int);
int symtable_insert_real(double);
int symtable_addtype(int, vartype, int);
int symtable_setarray(int, bool, int, int);

char* newString(char*);
int newTemp();
int newLabel();
void setProgramLabel(int);

vartype typePointer(vartype);
vartype simpleType(vartype);
int typeWide(vartype);
int getSymbolTableSize();
symtable_struct* newSymtable(symtable_struct*);
void delSymtable(symtable_struct*);
void freeAllocedSymtables();

int getSubProgramParameter(symtable_struct*, int, bool);
int getSubProgramParametersCount(symtable_struct*, bool);

int checkOvershadowing(const char*, entry_type);