#include "global.h"

int main(int argc, char* argv[]) {
	FILE* f = NULL;
	--argc; ++argv; /* skip program name */
	init();
	if(argc > 0) {
		f = fopen(argv[0],"r");
		lchangein(f);
		if(argc > 1) {
			out = fopen(argv[1],"w+");
		}
		else {
			out = stdout;
		}
	}
	else {
		fprintf(stderr, "Brak pliku wejsciowego.\n");
		return (EXIT_FAILURE);
	}
	parse();
	dumpAllLocalSymbolTables();
	dumpGlobalSymbolTable();
	freeAllocedSymtables();
	if(f != NULL)
		fclose(f);
	if(out != stdin)
		fclose(out);
	return 0;
}